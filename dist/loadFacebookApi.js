'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; //      

var _loadScript = require('load-script');

var _loadScript2 = _interopRequireDefault(_loadScript);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = function (emitter) {
  /**
   * A promise that is resolved when window.fbAsyncInit is called.
   * The promise is resolved with a reference to window.FB object.
   */
  var iframeAPIReady = new Promise(function (resolve) {
    if (window.FB && document.getElementById('facebook-jssdk')) {
      resolve(window.FB);

      return;
    } else {
      var protocol = window.location.protocol === 'http:' ? 'http:' : 'https:';

      var debug = false;
      if ((typeof process === 'undefined' ? 'undefined' : _typeof(process)) === 'object' && process.env) {
        debug = process.env.NODE_ENV === 'development';
      }
      (0, _loadScript2.default)(protocol + '//connect.facebook.net/en_US/sdk' + (debug ? '/debug.js' : '.js'), { "attrs": { "id": "facebook-jssdk" } }, function (error) {
        if (error) {
          emitter.trigger('error', error);
        }
      });
    }

    var previous = window.fbAsyncInit;

    // The API will call this function when page has finished downloading
    // the JavaScript for the player API.
    window.fbAsyncInit = function () {
      FB.init({ "version": "v2.7" });

      if (previous) {
        previous();
      }

      resolve(window.FB);
    };
  });

  return iframeAPIReady;
};

module.exports = exports['default'];