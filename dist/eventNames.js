'use strict';

Object.defineProperty(exports, "__esModule", {
   value: true
});
//      

/**
 * @see https://developers.facebook.com/docs/plugins/embedded-video-player/api/#event-reference
 */
exports.default = ['startedPlaying', 'paused', 'finishedPlaying', 'startedBuffering', 'finishedBuffering'];
module.exports = exports['default'];