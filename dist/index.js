'use strict';

Object.defineProperty(exports, "__esModule", {
  value: true
});

var _typeof = typeof Symbol === "function" && typeof Symbol.iterator === "symbol" ? function (obj) { return typeof obj; } : function (obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; };

var _slicedToArray = function () { function sliceIterator(arr, i) { var _arr = []; var _n = true; var _d = false; var _e = undefined; try { for (var _i = arr[Symbol.iterator](), _s; !(_n = (_s = _i.next()).done); _n = true) { _arr.push(_s.value); if (i && _arr.length === i) break; } } catch (err) { _d = true; _e = err; } finally { try { if (!_n && _i["return"]) _i["return"](); } finally { if (_d) throw _e; } } return _arr; } return function (arr, i) { if (Array.isArray(arr)) { return arr; } else if (Symbol.iterator in Object(arr)) { return sliceIterator(arr, i); } else { throw new TypeError("Invalid attempt to destructure non-iterable instance"); } }; }();

var _v = require('uuid/v4');

var _v2 = _interopRequireDefault(_v);

var _sister = require('sister');

var _sister2 = _interopRequireDefault(_sister);

var _eventNames = require('./eventNames');

var _eventNames2 = _interopRequireDefault(_eventNames);

var _loadFacebookApi = require('./loadFacebookApi');

var _loadFacebookApi2 = _interopRequireDefault(_loadFacebookApi);

var _FacebookVideoPlayer = require('./FacebookVideoPlayer');

var _FacebookVideoPlayer2 = _interopRequireDefault(_FacebookVideoPlayer);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var facebookAPI = void 0;

/**
 * A factory function used to produce an instance of the facebook playerand queue function calls and proxy events of the resulting object.
 *
 * @param elementId Either An existing YT.Player instance
 * the DOM element or the id of the HTML element where the API will insert an <iframe>.
 * @param options See `options` (Ignored when using an existing YT.Player instance).
 * @param strictState A flag designating whether or not to wait for
 * an acceptable state when calling supported functions. Default: `false`.
 * See `FunctionStateMap.js` for supported functions and acceptable states.
 */

exports.default = function (maybeElementId) {
  var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

  var emitter = (0, _sister2.default)();

  if (!facebookAPI) {
    facebookAPI = (0, _loadFacebookApi2.default)(emitter);
  }

  if (options.events) {
    throw new Error('Event handlers cannot be overwritten.');
  }

  if (typeof maybeElementId === 'string' && !document.getElementById(maybeElementId)) {
    throw new Error('Element "' + maybeElementId + '" does not exist.');
  }

  var playerAPIReady = new Promise(function (resolve) {
    if (typeof maybeElementId === 'string' || maybeElementId instanceof HTMLElement) {
      // eslint-disable-next-line promise/catch-or-return
      facebookAPI.then(function (FB) {
        var playerDiv = document.createElement('div');
        Object.entries(options).forEach(function (_ref) {
          var _ref2 = _slicedToArray(_ref, 2),
              key = _ref2[0],
              val = _ref2[1];

          playerDiv.dataset[key] = val;
        });
        playerDiv.classList.add("fb-video");
        playerDiv.id = (0, _v2.default)();
        var container = typeof maybeElementId === 'string' ? document.getElementById(maybeElementId) : maybeElementId;
        while (container.firstChild) {
          container.firstChild.remove();
        }
        container.appendChild(playerDiv);
        FB.Event.subscribe('xfbml.ready', function xfbmlReady(msg) {
          if (msg.type === 'video' && msg.id === playerDiv.id) {
            var _loop = function _loop(eventName) {
              msg.instance.subscribe(eventName, function (event) {
                emitter.trigger(eventName, event);
              });
            };

            var _iteratorNormalCompletion = true;
            var _didIteratorError = false;
            var _iteratorError = undefined;

            try {
              for (var _iterator = _eventNames2.default[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
                var eventName = _step.value;

                _loop(eventName);
              }
            } catch (err) {
              _didIteratorError = true;
              _iteratorError = err;
            } finally {
              try {
                if (!_iteratorNormalCompletion && _iterator.return) {
                  _iterator.return();
                }
              } finally {
                if (_didIteratorError) {
                  throw _iteratorError;
                }
              }
            }

            resolve(msg.instance);
            FB.Event.unsubscribe('xfbml.ready', xfbmlReady);
          }
        });
        FB.XFBML.parse(container);

        return null;
      });
    } else if ((typeof maybeElementId === 'undefined' ? 'undefined' : _typeof(maybeElementId)) === 'object' && maybeElementId.play instanceof Function) {
      var player = maybeElementId;

      resolve(player);
    } else {
      throw new TypeError('Unexpected state.');
    }
  });

  var playerApi = _FacebookVideoPlayer2.default.promisifyPlayer(playerAPIReady, emitter);

  return playerApi;
};

module.exports = exports['default'];