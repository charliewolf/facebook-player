'use strict';

Object.defineProperty(exports, "__esModule", {
   value: true
});
//      

/**
 * @see https://developers.facebook.com/docs/plugins/embedded-video-player/api/#control-reference
 */
exports.default = ['play', 'pause', 'seek', 'mute', 'unmute', 'isMuted', 'setVolume', 'getVolume', 'getCurrentPosition', 'getDuration'];
module.exports = exports['default'];