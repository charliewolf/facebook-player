'use strict';

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _debug = require('debug');

var _debug2 = _interopRequireDefault(_debug);

var _functionNames = require('./functionNames');

var _functionNames2 = _interopRequireDefault(_functionNames);

var _eventNames = require('./eventNames');

var _eventNames2 = _interopRequireDefault(_eventNames);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var debug = (0, _debug2.default)('facebook-player');

var FacebookVideoPlayer = {};

/**
 * Delays player API method execution until player state is ready.
 *
 * @todo Proxy all of the methods using Object.keys.
 * @todo See TRICKY below.
 * @param playerAPIReady Promise that resolves when player is ready.
 * @param strictState A flag designating whether or not to wait for
 * an acceptable state when calling supported functions.
 * @returns {Object}
 */
FacebookVideoPlayer.promisifyPlayer = function (playerAPIReady, emitter) {
    var functions = {};

    var _loop = function _loop(functionName) {
        functions[functionName] = function () {
            for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
                args[_key] = arguments[_key];
            }

            return playerAPIReady.then(function (player) {
                // eslint-disable-next-line no-warning-comments
                // TODO: Just spread the args into the function once Babel is fixed:
                // https://github.com/babel/babel/issues/4270
                //
                // eslint-disable-next-line prefer-spread
                return player[functionName].apply(player, args);
            });
        };
    };

    var _iteratorNormalCompletion = true;
    var _didIteratorError = false;
    var _iteratorError = undefined;

    try {
        for (var _iterator = _functionNames2.default[Symbol.iterator](), _step; !(_iteratorNormalCompletion = (_step = _iterator.next()).done); _iteratorNormalCompletion = true) {
            var functionName = _step.value;

            _loop(functionName);
        }
    } catch (err) {
        _didIteratorError = true;
        _iteratorError = err;
    } finally {
        try {
            if (!_iteratorNormalCompletion && _iterator.return) {
                _iterator.return();
            }
        } finally {
            if (_didIteratorError) {
                throw _iteratorError;
            }
        }
    }

    functions['getDuration'] = function () {
        return playerAPIReady.then(function (player) {
            return new Promise(function getDuration(resolve) {
                var duration = player.getDuration();
                if (duration > 0) {
                    resolve(duration);
                } else {
                    setTimeout(getDuration.bind(null, resolve), 0);
                }
            });
        });
    };

    functions.getDuration().then(function () {
        emitter.trigger('ready');
    });
    functions.on = emitter.on;
    functions.off = emitter.off;
    return functions;
};

exports.default = FacebookVideoPlayer;
module.exports = exports['default'];