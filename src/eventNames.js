//      

/**
 * @see https://developers.facebook.com/docs/plugins/embedded-video-player/api/#event-reference
 */
export default [
   'startedPlaying',
   'paused',
   'finishedPlaying',
   'startedBuffering',
   'finishedBuffering'
];

