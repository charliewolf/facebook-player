import uuid from 'uuid/v4';
import Sister from 'sister';
import eventNames from './eventNames';
import loadFacebookApi from './loadFacebookApi';
import FacebookVideoPlayer from './FacebookVideoPlayer';
             

let facebookAPI;

/**
 * A factory function used to produce an instance of the facebook playerand queue function calls and proxy events of the resulting object.
 *
 * @param elementId Either An existing YT.Player instance
 * the DOM element or the id of the HTML element where the API will insert an <iframe>.
 * @param options See `options` (Ignored when using an existing YT.Player instance).
 * @param strictState A flag designating whether or not to wait for
 * an acceptable state when calling supported functions. Default: `false`.
 * See `FunctionStateMap.js` for supported functions and acceptable states.
 */
export default (maybeElementId, options = {}) => {
  const emitter = Sister();

  if (!facebookAPI) {
    facebookAPI = loadFacebookApi(emitter);
  }

  if (options.events) {
    throw new Error('Event handlers cannot be overwritten.');
  }

  if (typeof maybeElementId === 'string' && !document.getElementById(maybeElementId)) {
    throw new Error('Element "' + maybeElementId + '" does not exist.');
  }

  const playerAPIReady = new Promise((resolve                                           ) => {
    if (typeof maybeElementId === 'string' || maybeElementId instanceof HTMLElement) {
      // eslint-disable-next-line promise/catch-or-return
      facebookAPI
        .then((FB) => {
          const playerDiv = document.createElement('div');
          Object.entries(options).forEach(([key, val]) => {
		playerDiv.dataset[key] = val;
          });
          playerDiv.classList.add("fb-video");
          playerDiv.id = uuid();
          let container = typeof maybeElementId === 'string' ? document.getElementById(maybeElementId) : maybeElementId;
          while (container.firstChild) {
              container.firstChild.remove();
          }
          container.appendChild(playerDiv);
          FB.Event.subscribe('xfbml.ready', function xfbmlReady(msg) {
              if(msg.type === 'video' && msg.id === playerDiv.id){
                  for (const eventName of eventNames) {
                      msg.instance.subscribe(eventName, function(event){
                          emitter.trigger(eventName, event);
                      });
                  }
                  resolve(msg.instance);
                  FB.Event.unsubscribe('xfbml.ready', xfbmlReady);
              }
          });
          FB.XFBML.parse(container);

          return null;
        });
    } else if (typeof maybeElementId === 'object' && maybeElementId.play instanceof Function) {
      const player                          = maybeElementId;

      resolve(player);
    } else {
      throw new TypeError('Unexpected state.');
    }
  });

  const playerApi = FacebookVideoPlayer.promisifyPlayer(playerAPIReady, emitter);


  return playerApi;
};
