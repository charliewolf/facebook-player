import createDebug from 'debug';
import functionNames from './functionNames';
import eventNames from './eventNames';
             
              
const debug = createDebug('facebook-player');

const FacebookVideoPlayer = {};


/**
 * Delays player API method execution until player state is ready.
 *
 * @todo Proxy all of the methods using Object.keys.
 * @todo See TRICKY below.
 * @param playerAPIReady Promise that resolves when player is ready.
 * @param strictState A flag designating whether or not to wait for
 * an acceptable state when calling supported functions.
 * @returns {Object}
 */
FacebookVideoPlayer.promisifyPlayer = (playerAPIReady, emitter) => {
  const functions = {};

  for (const functionName of functionNames) {
      functions[functionName] = (...args) => {
        return playerAPIReady
        .then((player) => {
          // eslint-disable-next-line no-warning-comments
          // TODO: Just spread the args into the function once Babel is fixed:
          // https://github.com/babel/babel/issues/4270
          //
          // eslint-disable-next-line prefer-spread
          return player[functionName].apply(player, args);
        });
      };
  }

  functions['getDuration'] = function(){
      return playerAPIReady.then((player) => {
          return new Promise(function getDuration(resolve) {
              let duration = player.getDuration();
              if(duration > 0){
                  resolve(duration);                  
              } else {
                  setTimeout(getDuration.bind(null, resolve), 0);
              }
         });
      });      
  }  

  functions.getDuration().then(function(){
      emitter.trigger('ready');
  });
  functions.on = emitter.on;
  functions.off = emitter.off;
  return functions;
};

export default FacebookVideoPlayer;
