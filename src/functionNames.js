//      

/**
 * @see https://developers.facebook.com/docs/plugins/embedded-video-player/api/#control-reference
 */
export default [
   'play',
   'pause',
   'seek',
   'mute',
   'unmute',
   'isMuted',
   'setVolume',
   'getVolume',
   'getCurrentPosition',
   'getDuration'
];
