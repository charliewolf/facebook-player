//      

import load from 'load-script';
             
              
               
                 

export default (emitter             )                         => {
  /**
   * A promise that is resolved when window.fbAsyncInit is called.
   * The promise is resolved with a reference to window.FB object.
   */
  const iframeAPIReady = new Promise((resolve) => {
    if (window.FB && document.getElementById('facebook-jssdk')) {
      resolve(window.FB);

      return;
    } else {
      const protocol = window.location.protocol === 'http:' ? 'http:' : 'https:';

      let debug = false;
      if(typeof process === 'object' && process.env){
          debug = process.env.NODE_ENV === 'development';
      }
      load(protocol + '//connect.facebook.net/en_US/sdk' + (debug ? '/debug.js' : '.js'), {"attrs": {"id": "facebook-jssdk"}}, (error) => {
        if (error) {
          emitter.trigger('error', error);
        }
      });
    }

    const previous = window.fbAsyncInit;

    // The API will call this function when page has finished downloading
    // the JavaScript for the player API.
    window.fbAsyncInit = () => {
      FB.init({"version": "v2.7"});

      if (previous) {
        previous();
      }

      resolve(window.FB);
    };
  });

  return iframeAPIReady;
};
